# Hexea

This is a simple library for working with the closely related connection games of [Y](https://en.wikipedia.org/wiki/Y_(game)) and [Hex](https://en.wikipedia.org/wiki/Hex_(board_game)).  This is not meant to be a standalone, playable game, but rather a set of tools one could use to implement such a game, train a machine learning model to play a game, and so forth.

Full documentation can be found [here](https://cholling.com/hexea).

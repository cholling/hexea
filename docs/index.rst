hexea - Python library for the games of Y and Hex
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   examples
   reference

`Repository <https://codeberg.org/cholling/hexea>`_

Overview
________
``hexea`` is a library for representing the related games of `Y <https://en.wikipedia.org/wiki/Y_(game)>`_ and `Hex <https://en.wikipedia.org/wiki/Hex_(board_game)>`_.  It consists of classes representing Y and Hex boards, along with methods for calculating the winner (if any) of a given board, generating random playouts, and other useful features.  ``hexea`` does not, by itself, provide any capability of actually playing a game, but could be used as a building block for a game-playing program.  It also provides tools that could be useful for training a Hex- or Y-playing machine learning model.

Introduction
------------
The primary data structure provided by ``hexea`` is the :py:class:`Board <hexea.Board>` protocol.  Both the :py:class:`Yboard <hexea.Yboard>` and :py:class:`Hexboard <hexea.Hexboard>` classes implement this protocol.  Boards can be created of any size greater than or equal to 1 (limited, of course, by your system's ``maxint`` and memory constraints).

The other important class provided by ``hexea`` is the :py:class:`Marker <hexea.Marker>` class, an ``Enum`` class enumerating the possible contents of a hex (``red``, ``blue``, or ``none``).

At this point, some :doc:`examples` might be more illustrative:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Examples
========

.. code:: ipython3

    from hexea import Yboard, Hexboard
    
    yb = Yboard(5)
    print(yb)


.. parsed-literal::

    
    .
       .
    .     .
       .     .
    .     .     .
       .     .
    .     .
       .
    .
    


.. code:: ipython3

    # method chaining
    print (
        yb
        .move(2, 1)
        .move(1, 1)
    )


.. parsed-literal::

    
    .
       .
    .     .
       O     .
    .     X     .
       .     .
    .     .
       .
    .
    


.. code:: ipython3

    # move modifies in-place
    print(yb)


.. parsed-literal::

    
    .
       .
    .     .
       O     .
    .     X     .
       .     .
    .     .
       .
    .
    


.. code:: ipython3

    from copy import copy
    
    yb2 = copy(yb)
    print(yb.random_playout())


.. parsed-literal::

    
    O
       O
    O     X
       O     X
    X     X     X
       X     O
    O     X
       X
    O
    


.. code:: ipython3

    print(yb2)


.. parsed-literal::

    
    .
       .
    .     .
       O     .
    .     X     .
       .     .
    .     .
       .
    .
    


.. code:: ipython3

    hb = Hexboard(5)
    
    print(hb)


.. parsed-literal::

    
     xxxxxxxxxxxxxxxxxxx
    o .   .   .   .   . o
     o                   o
      o .   .   .   .   . o
       o                   o
        o .   .   .   .   . o
         o                   o
          o .   .   .   .   . o
           o                   o
            o .   .   .   .   . o
             xxxxxxxxxxxxxxxxxxx
    


.. code:: ipython3

    print(hb.random_playout(quick=False))


.. parsed-literal::

    
     xxxxxxxxxxxxxxxxxxx
    o X   .   O   .   O o
     o                   o
      o X   X   X   O   X o
       o                   o
        o X   O   .   X   O o
         o                   o
          o X   O   O   O   X o
           o                   o
            o O   .   X   O   . o
             xxxxxxxxxxxxxxxxxxx
    


.. code:: ipython3

    print(hb.get_winner())


.. parsed-literal::

    O


.. code:: ipython3

    hb2 = Hexboard(3)
    l = hb2.get_list_of_dicts(5)
    print(l)


.. parsed-literal::

    [{'cell0_0': <Marker.red: 1>, 'cell1_0': <Marker.red: 1>, 'cell0_1': <Marker.red: 1>, 'cell2_0': <Marker.blue: -1>, 'cell1_1': <Marker.red: 1>, 'cell0_2': <Marker.blue: -1>, 'cell2_1': <Marker.blue: -1>, 'cell1_2': <Marker.blue: -1>, 'cell2_2': <Marker.red: 1>, 'winner': <Marker.blue: -1>}, {'cell0_0': <Marker.red: 1>, 'cell1_0': <Marker.red: 1>, 'cell0_1': <Marker.blue: -1>, 'cell2_0': <Marker.blue: -1>, 'cell1_1': <Marker.blue: -1>, 'cell0_2': <Marker.red: 1>, 'cell2_1': <Marker.red: 1>, 'cell1_2': <Marker.red: 1>, 'cell2_2': <Marker.blue: -1>, 'winner': <Marker.blue: -1>}, {'cell0_0': <Marker.red: 1>, 'cell1_0': <Marker.blue: -1>, 'cell0_1': <Marker.blue: -1>, 'cell2_0': <Marker.red: 1>, 'cell1_1': <Marker.red: 1>, 'cell0_2': <Marker.blue: -1>, 'cell2_1': <Marker.red: 1>, 'cell1_2': <Marker.blue: -1>, 'cell2_2': <Marker.red: 1>, 'winner': <Marker.red: 1>}, {'cell0_0': <Marker.blue: -1>, 'cell1_0': <Marker.red: 1>, 'cell0_1': <Marker.red: 1>, 'cell2_0': <Marker.red: 1>, 'cell1_1': <Marker.blue: -1>, 'cell0_2': <Marker.red: 1>, 'cell2_1': <Marker.blue: -1>, 'cell1_2': <Marker.red: 1>, 'cell2_2': <Marker.blue: -1>, 'winner': <Marker.red: 1>}, {'cell0_0': <Marker.blue: -1>, 'cell1_0': <Marker.blue: -1>, 'cell0_1': <Marker.blue: -1>, 'cell2_0': <Marker.red: 1>, 'cell1_1': <Marker.red: 1>, 'cell0_2': <Marker.red: 1>, 'cell2_1': <Marker.red: 1>, 'cell1_2': <Marker.red: 1>, 'cell2_2': <Marker.blue: -1>, 'winner': <Marker.red: 1>}]


.. code:: ipython3

    import pandas as pd
    df = pd.DataFrame.from_records(l)
    df




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>cell0_0</th>
          <th>cell1_0</th>
          <th>cell0_1</th>
          <th>cell2_0</th>
          <th>cell1_1</th>
          <th>cell0_2</th>
          <th>cell2_1</th>
          <th>cell1_2</th>
          <th>cell2_2</th>
          <th>winner</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
          <td>O</td>
          <td>O</td>
          <td>O</td>
          <td>X</td>
          <td>O</td>
        </tr>
        <tr>
          <th>1</th>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>O</td>
          <td>O</td>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>O</td>
        </tr>
        <tr>
          <th>2</th>
          <td>X</td>
          <td>O</td>
          <td>O</td>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
          <td>X</td>
        </tr>
        <tr>
          <th>3</th>
          <td>O</td>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
        </tr>
        <tr>
          <th>4</th>
          <td>O</td>
          <td>O</td>
          <td>O</td>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>X</td>
          <td>O</td>
          <td>X</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    # For ML training we probably want integer values instead of Marker
    df.astype(int)




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>cell0_0</th>
          <th>cell1_0</th>
          <th>cell0_1</th>
          <th>cell2_0</th>
          <th>cell1_1</th>
          <th>cell0_2</th>
          <th>cell2_1</th>
          <th>cell1_2</th>
          <th>cell2_2</th>
          <th>winner</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
          <td>-1</td>
          <td>-1</td>
          <td>-1</td>
          <td>1</td>
          <td>-1</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>-1</td>
          <td>-1</td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>-1</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1</td>
          <td>-1</td>
          <td>-1</td>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
          <td>1</td>
        </tr>
        <tr>
          <th>3</th>
          <td>-1</td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
        </tr>
        <tr>
          <th>4</th>
          <td>-1</td>
          <td>-1</td>
          <td>-1</td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>1</td>
          <td>-1</td>
          <td>1</td>
        </tr>
      </tbody>
    </table>
    </div>



